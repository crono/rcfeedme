﻿/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ServiceModel.Syndication;
using System.Net;
using System.Xml;
using System.IO;
using RcSoft.RcCommon;
using System.Xml.Linq;
using System.Globalization;
using System.Web;

namespace rcFeedMe
{
    public class Feed
    {
        public event EventHandler FinishedRefresh;        

        public enum StatusEnum
        {
            Init,
            Refreshing,
            Refreshed,
            Error,
            Ignore
        }       
        public string Name = "";
        public string Url = "";
        public string GroupName = "";
        public int KeepCount = 10;
        public string IncludeExt = "";
        public string IncludeText = "";
        public string SavePath = "";
        public string SyncPath = "";
        public bool Prepend = false;
        public bool PrependDate = false;
        public bool PrependShortDate = false;
        public bool DoSync = false;
        public bool DoCacheFix = false;
        public string Username = "";
        public string Password = "";
        public int? Priority = null;
        public bool DoDecode = false;
        public string PostDLExe = "";
        public string PostDLArgs = "";
        public string PreSyncExe = "";
        public string PreSyncArgs = "";
        public bool SuppressSyncCopy = false;

        [XmlIgnore]
        public string ErrorMsg = "";
        [XmlIgnore]
        public StatusEnum Status = StatusEnum.Init;
        [XmlIgnore]
        public int Count = 0;
        [XmlIgnore]
        public SyndicationFeed SynFeed;
        [XmlIgnore]
        public MediaFeed MediaCollection = new MediaFeed();                
        [XmlIgnore]
        public Config cfg;
        [XmlIgnore]
        public bool DumpRawXml = false;
        [XmlIgnore]
        public Group group;

        //private RcWebAsync request = new RcWebAsync();
        private RcWebAsync request = null;


        public Feed()
        {
//            request.DownloadStringCompleted += new DownloadStringCompletedEventHandler(request_DownloadStringCompleted);
        }

        public Feed(Config c)
        {
            cfg = c;
  //          request.DownloadStringCompleted += new DownloadStringCompletedEventHandler(request_DownloadStringCompleted);
        }

        public static string GetTitle(string url)
        {            
            string ret = RcSharedWeb.GetUrl(url);
            XmlReader reader = XmlReader.Create(new StringReader(ret));
            SyndicationFeed sf;
            try
            {
                sf = SyndicationFeed.Load(reader);
            }
            catch (System.Exception ex)
            {
                ret = CleanupConfig.CleanupXMLAll(ret, url);                
                reader = XmlReader.Create(new StringReader(ret));
                sf = SyndicationFeed.Load(reader);
            }
            return sf.Title.Text;            
        }

        public void ConsumeFeed()
        {            
            if (request == null)
            {
                request = new RcWebAsync();
                request.DownloadStringCompleted += new DownloadStringCompletedEventHandler(request_DownloadStringCompleted);
            }
            RcShared.WriteLog("(Feed)Start feed refresh: " + this.Name);
            this.Status = StatusEnum.Refreshing;
            //request.DownloadProgressChanged += new DownloadProgressChangedEventHandler(request_DownloadProgressChanged);            
            request.TimeoutSec = cfg.ScanTimeout;
            request.StallSec = cfg.ScanStall;
            request.Retry = cfg.ScanRetry;
            request.DoCacheFix = this.DoCacheFix;
            
            request.DownloadStringAsync(this.Url,null,this.Username, this.Password);            
        }

        //void request_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        //{
            
        //}      

        void request_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            string clean = "";
            try
            {                
                clean = e.Result;
                //clean = RcSharedWeb.GetUrl(this.Url, cfg.ScanStall * 1000);     
                //rcGetWebObj wo = new rcGetWebObj();
                //clean = wo.GetWeb(this.Url);
                if (DumpRawXml)
                {
                    //string winstr = RcShared.Unix2Dos(e.Result);
                    RcShared.OWriteLog(RcShared.GetExePath(false) + @"\" + "Feed - " + RcShared.ScrubFileName(this.Name) + ".xml", clean);
                }

                //dc:date fix
                if (clean.IndexOf("<pubDate>") == -1 && clean.IndexOf("<dc:date>") != -1)
                {
                    clean = clean.Replace("<dc:date>", "<pubDate>");
                    clean = clean.Replace("</dc:date>", "</pubDate>");
                }

                XmlReader reader = XmlReader.Create(new StringReader(clean));
                try
                {                    
                    SynFeed = SyndicationFeed.Load(reader);                    

                    ////quick url check for lost
                    //bool retrySynFeed = false;                    

                    //foreach (SyndicationItem si in SynFeed.Items)
                    //{
                    //    foreach (SyndicationLink sl in si.Links)
                    //    {
                    //        if (sl.MediaType != null)
                    //        {
                    //            try
                    //            {
                    //                string strcCheck = sl.Uri.AbsoluteUri;
                    //            }
                    //            catch (System.Exception ex)
                    //            {                                    
                    //                XmlDocument xml = new XmlDocument();
                    //                xml.LoadXml(clean);
                    //                XmlNodeList nodes = xml.SelectNodes("/rss/channel/item/link");                                    
                    //                if (nodes.Count > 0)                                    
                    //                {
                    //                    throw ex;
                    //                }
                    //                if (clean.IndexOf("<enclosure url=") == -1)
                    //                {
                    //                    throw ex;
                    //                }
                    //                nodes = xml.SelectNodes("/rss/channel/item");
                    //                foreach (XmlNode x in nodes)
                    //                {
                    //                    XmlNode enc = x.SelectSingleNode("enclosure");
                    //                    if (enc != null)
                    //                    {
                    //                        string senc = enc.Attributes["url"].Value;
                    //                        if (senc.StartsWith(@"http"))
                    //                        {                                                
                    //                            senc = HttpUtility.HtmlEncode(senc);
                    //                            enc.Attributes["url"].Value = senc;
                    //                            XmlElement elem = xml.CreateElement("link");                                                
                    //                            elem.InnerText = senc;                                                
                    //                            x.AppendChild(elem);
                    //                        }
                    //                    }
                    //                }
                    //                clean = xml.OuterXml;                                
                    //                retrySynFeed = true;
                    //                break;
                    //            }
                    //        }
                    //    }
                    //    if (retrySynFeed)
                    //        break;
                    //}
                    //if (retrySynFeed)
                    //{
                    //    reader = XmlReader.Create(new StringReader(clean));
                    //    SynFeed = SyndicationFeed.Load(reader);
                    //}
                }
                catch (System.Xml.XmlException exx)
                {
                    //try and cleanup crap feeds                
                    clean = CleanupConfig.CleanupXMLAll(clean, this.Url);

                    if (DumpRawXml)
                    {
                        //string winstr = RcShared.Unix2Dos(clean);
                        RcShared.OWriteLog(RcShared.GetExePath(false) + @"\" + "Feed (initial cleanup) - " + RcShared.ScrubFileName(this.Name) + ".xml", clean);
                    }    

                    XmlDocument xml = new XmlDocument();
                    try
                    {
                        xml.LoadXml(clean);
                    }
                    catch (System.Exception exLoad)
                    {                        
                        //make a sync call using httpwebrequest to see if it makes a difference.
                        clean = RcSharedWeb.GetUrl(this.Url,cfg.ScanStall * 1000);
                        clean = CleanupConfig.CleanupXMLAll(clean, this.Url);
                        xml.LoadXml(clean);
                    }
                    XmlNodeList nodes = xml.SelectNodes("/rss/channel/item/pubDate");
                    foreach (XmlNode x in nodes)
                    {
                        System.DateTime dt = ParseDt(x.InnerText);
                        //fixes microsoft bug with parsing dates in DateTimeFormatInfo.RFC1123Pattern format
                        string n = dt.ToString("r");
                        x.InnerText = n;
                    }
                    nodes = xml.SelectNodes("/rss/channel/lastBuildDate");
                    foreach (XmlNode x in nodes)
                    {
                        System.DateTime dt = ParseDt(x.InnerText);
                        //fixes microsoft bug with parsing dates in DateTimeFormatInfo.RFC1123Pattern format
                        string n = dt.ToString("r");
                        x.InnerText = n;
                    }
                    clean = xml.OuterXml;
                    reader = XmlReader.Create(new StringReader(clean));
                    SynFeed = SyndicationFeed.Load(reader);                           
                }

                MediaCollection.Clear();

                if (DumpRawXml)
                {
                    //string winstr = RcShared.Unix2Dos(clean);
                    RcShared.OWriteLog(RcShared.GetExePath(false) + @"\" + "Feed (fixed) - " + RcShared.ScrubFileName(this.Name) + ".xml", clean);
                }               

                foreach (SyndicationItem si in SynFeed.Items)
                {
                    foreach (SyndicationLink sl in si.Links)
                    {
                        //fix the case where mediatype is null but there is a valid enclosure
                        //this is because the <enclosure> tag in the feed is missing the type= attribute
                        if (sl.MediaType == null)
                        {
                            string checkstr = "";
                            try
                            {
                                checkstr = sl.Uri.AbsoluteUri;
                            }
                            catch (System.Exception ex)
                            {
                            }
                            if (checkstr != String.Empty)
                            {
                                string ext = checkstr.Substring(checkstr.Length - 4);
                                ext = ext.ToUpper();
                                if (ext == ".MP3" || ext == ".M4A" || ext == ".MP4" || ext == ".M4V" || ext == ".MOV")
                                {
                                    //ok looks like a valid enclosure
                                    sl.MediaType = "media";
                                }
                            }
                        }

                        if (sl.MediaType != null)
                        {
                            try
                            {
                                string checkstr = sl.Uri.AbsoluteUri;
                            }
                            catch (System.Exception exu)
                            {
                                RcShared.WriteWarn("(Feed)Link to file seems to be missing: " + si.Title.Text + "; " + exu.Message);
                                continue;
                            }

                            Media m = new Media();

                            m.FeedName = this.Name;
                            m.PostDLExe = this.PostDLExe;
                            m.PostDLArgs = this.PostDLArgs;
                            if (this.group == null)
                                m.GroupName = "";
                            else
                                m.GroupName = this.group.Name;
                            m.SynchPath = this.SyncPath;
                            m.SuppressSyncCopy = this.SuppressSyncCopy;
                            m.PreSyncExe = this.PreSyncExe;
                            m.PreSyncArgs = this.PreSyncArgs;                                

                            m.Title = RcShared.StripHTML(si.Title.Text).Trim();
                            if (si.Summary != null)
                            {
                                m.Summary = RcShared.StripHTML(si.Summary.Text);          
                            }
                            //generate filename
                            m.Link = sl.Uri.AbsoluteUri;
                            string s = sl.Uri.AbsoluteUri;
                            int i = s.LastIndexOf(@"/");
                            s = s.Substring(i + 1);
                            s = s.Trim();
                            i = s.IndexOf("?");
                            if (i > 0)
                            {
                                s = s.Substring(0, i);
                            }
                            m.FileName = s.Trim();
                            if (this.DoDecode)
                                m.FileName = System.Web.HttpUtility.UrlDecode(m.FileName);
                            m.FileName = RcShared.ScrubFileName(m.FileName);
                            if (this.Prepend)
                            {
                                m.FileName = m.Title + "-" + m.FileName;
                                m.FileName = RcShared.ScrubFileName(m.FileName);
                            }
                            m.PubDate = si.PublishDate.DateTime;
                            if (this.PrependDate)
                            {
                                m.FileName = m.PubDate.ToString("yyyyMMdd") + "-" + m.FileName;
                                m.FileName = RcShared.ScrubFileName(m.FileName);
                            }
                            else if (this.PrependShortDate)
                            {
                                m.FileName = m.PubDate.ToString("M.d") + "-" + m.FileName;
                                m.FileName = RcShared.ScrubFileName(m.FileName);
                            }
                            //check length
                            //m.FileName = Path.GetFileNameWithoutExtension(m.FileName) + "123456789012345678901234567890123456789012345678901234567890.  the quick brown fox jumps over the lazy dogs.  in the end you will submit its got to hurt a little bit. when in danger or in doubt run in circles scream and shout" + Path.GetExtension(m.FileName);
                            string pth = this.Resolvepath(this.group);
                            int maxlen = 240 - pth.Length;                                                             
                            if (m.FileName.Length > maxlen)
                            {
                                m.FileName = Path.GetFileNameWithoutExtension(m.FileName).Substring(0,maxlen) + Path.GetExtension(m.FileName);
                                RcShared.WriteLog("filename exceeded max length: " + maxlen.ToString() + ".  Trimming filename -> " + m.FileName);
                            }

                            m.Status = Media.StatusEnum.Ignore;                            

                            try
                            {
                                if (m.Title != String.Empty && m.FileName != String.Empty)
                                {                                 
                                    if (MediaCollection.ContainsNoCase(m.PK))
                                    {
                                        if (cfg.LogDupErrors)
                                            RcShared.WriteWarn("(feed)[" + this.Name + "]" + "Media already exists in collection (a duplicate podcast was found in the feed and ignored); " + m.PK);
                                    }
                                    else
                                    {
                                        MediaCollection.Add(m);
                                    }
                                }
                                else
                                {
                                    RcShared.WriteWarn("(feed)[" + this.Name + ";" + m.PK + "]Media had either missing title or filename.");
                                }
                            }
                            //catch (System.ArgumentException ex)
                            //{
                            //    m.FileName = m.Title + "-" + m.FileName;
                            //    m.FileName = RcShared.ScrubFileName(m.FileName);
                            //    try
                            //    {
                            //        MediaCollection.Add(m);
                            //    }
                            //    catch (System.Exception ex2)
                            //    {
                            //        RcShared.WriteWarn("(feed)[" + this.Name + "]" + ex2.Message);
                            //    }
                            //}
                            catch (System.Exception ex)
                            {
                                RcShared.WriteWarn("(feed)[" + this.Name + "]" + ex.Message + "; " + m.PK);
                            }                            
                        }
                    }
                }

                MediaCollection.Sort();
                //foreach (Media m1 in MediaCollection)
                //{
                //    RcShared.WriteLog(m1.PK);
                //}

                ErrorMsg = "";
                Count = MediaCollection.Count;
                Status = StatusEnum.Refreshed;

                RcShared.WriteLog("(Feed)Finished feed refresh: " + this.Name + " = " + Count.ToString());

                request = null;
                if (this.FinishedRefresh != null)
                    this.FinishedRefresh(this, new EventArgs());
            }
            catch (System.Exception ex)
            {
                try
                {
                    ErrorMsg = ex.Message;
                    Count = 0;
                    Status = StatusEnum.Error;
                    string inner = "";
                    if (ex.InnerException != null)
                        inner = ex.InnerException.Message;
                    string errStr = "";
                    if (ex.Message != String.Empty && ex.Message.StartsWith("Error in line 1 position "))
                    {
                        int startErr = "Error in line 1 position ".Length;
                        int stopErr = ex.Message.IndexOf(".",startErr);
                        if (stopErr > 0)
                        {
                            string subErr = ex.Message.Substring(startErr, stopErr - startErr);
                            if (RcShared.IsNumeric(subErr))
                            {
                                int errPos = Convert.ToInt32(subErr);
                                if (errPos > 60)
                                    errStr = clean.Substring(errPos - 60, 120);
                            }
                        }
                    }
                    RcShared.WriteError("(Feed)[" + this.Name + "]" + ex.Message + "; " + inner + "; " + errStr);
                    if (request != null && request.CanRetry())
                    {
                        ConsumeFeed();
                    }
                    else
                    {
                        this.MediaCollection.Clear();
                        RcShared.WriteLog("(Feed)Finished feed refresh with ERRORS: " + this.Name + " = " + Count.ToString());
                        request = null;
                        if (this.FinishedRefresh != null)
                            this.FinishedRefresh(this, new EventArgs());
                    }
                }
                catch (System.Exception ex2)
                {
                    RcShared.WriteEx(ex2);
                }
            }                      
        }

        private System.DateTime ParseDt(string strDt)
        {
            string ori = strDt;
            string errmsg = "";
            System.DateTime ret = Convert.ToDateTime(@"01/01/1950");
            
            //first try any custom defined date format strings
            string[] ary = cfg.DateFormats.Split("|".ToCharArray());
            CultureInfo provider = CultureInfo.InvariantCulture;

            foreach (string s in ary)
            {
                try
                {
                    ret = System.DateTime.ParseExact(strDt, s,provider);
                    return ret;
                }
                catch (System.Exception ex)
                {                    
                }                                
            }

            //try ripping out the time zone
            ary = cfg.TimeZones.Split("|".ToCharArray());
            foreach (string s in ary)
            {
                strDt = strDt.Replace(s, "");
            }
        
            strDt = strDt.Trim();
            try
            {
                ret = System.DateTime.Parse(strDt);
                return ret;                    
            }
            catch (System.Exception ex)
            {
            }
                                    
            //strip out day of the week
            int firstComma = strDt.IndexOf(",");
            if (firstComma == -1)
            {
                firstComma = strDt.IndexOf(" ");
            }
            if (firstComma != -1)
            {
                strDt = strDt.Substring(firstComma + 1).Trim();
                try
                {
                    ret = System.DateTime.Parse(strDt);
                    return ret;
                }
                catch (System.Exception ex)
                {
                    errmsg = ex.Message;
                }
            }

            //try one more time with custom date formats after removing above elements.
            ary = cfg.DateFormats.Split("|".ToCharArray());            
            foreach (string s in ary)
            {
                try
                {
                    ret = System.DateTime.ParseExact(strDt, s, provider);
                    return ret;
                }
                catch (System.Exception ex)
                {
                }
            }


            RcShared.WriteWarn("(Feed - DateTime.Parse)" + this.Name + "; Failed to parse publish date; going to use 01/01/1950 so it sorts to the bottom; " + errmsg + ";problem string=" + ori);
            return ret;
        }
 

        //public bool CheckGenericFileName(string s)
        //{
        //    string[] ary = cfg.GenericFileNames.Split("|".ToCharArray());
        //    foreach (string g in ary)
        //    {
        //        if (s.ToUpper() == g.ToUpper())
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        public string Resolvepath(Group g)
        {            
            string basePath = "";
            string finalPath = "";
            string fname = RcShared.ScrubFileName(this.Name);

            if (this.SavePath != String.Empty)
            {
                basePath = this.SavePath;
                finalPath = basePath;
            }
            else if (g != null && this.GroupName != String.Empty)
            {
                basePath = g.SavePath;
                finalPath = basePath + @"\" + fname;
            }
            else
            {
                basePath = cfg.SavePath;
                finalPath = basePath + @"\" + fname;
            }

            if (!Directory.Exists(basePath))
                throw new ApplicationException("Path doesn't exist: " + basePath);
          
            return finalPath;
        }

        public string ResolveSyncPath(Group g)
        {
            string basePath = "";
            string finalPath = "";
            string fname = RcShared.ScrubFileName(this.Name);

            if (this.SyncPath != String.Empty)
            {
                basePath = this.SyncPath;
                finalPath = basePath;
            }
            else if (g != null && this.GroupName != String.Empty)
            {
                basePath = g.SyncPath;
                if (!g.BareSyncPath) 
                    finalPath = basePath + @"\" + fname;
                else
                    finalPath = basePath;
            }
            else
            {
                basePath = cfg.SyncPath;
                finalPath = basePath + @"\" + fname;
            }

            if (!Directory.Exists(finalPath))
                Directory.CreateDirectory(finalPath);

            return finalPath;
        }

        public void Stop()
        {
            this.Status = StatusEnum.Ignore;
            request.Abort();
        }
    }
}


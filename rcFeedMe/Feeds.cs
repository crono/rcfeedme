﻿/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using RcSoft.RcCommon;
using System.Xml.Serialization;

namespace rcFeedMe
{
    public class Feeds : System.Collections.ObjectModel.KeyedCollection<string,Feed>
    {
        public event EventHandler FinishedRefresh;
        public event EventHandler ProgressChanged;

        public const string XMLName = "Feeds.xml";
        public readonly string XMLFullName = RcShared.GetExePath(false) + @"\" + Feeds.XMLName;

        private const string encryptTag = "[encrypted]";

        [XmlIgnore]
        public int FeedsRefreshed = 0;
        [XmlIgnore]
        public bool DumpRawXml = false;

        private Groups grps;

        public Groups Grps
        {
            get { return grps; }
            set 
            { 
                grps = value;
                foreach (Feed f in this)
                {
                    if (f.GroupName != String.Empty && grps != null && grps.Count > 0)
                        f.group = grps[f.GroupName];
                }
            }
        }
        private Config cfg;

        private bool isStopAll = false;

        public Config Cfg
        {
            get { return cfg; }
            set 
            { 
                cfg = value;
                foreach (Feed f in this)
                {
                    f.cfg = cfg;
                }
            }
        }
        

        public Feeds()
        {
           
        }

        public Feeds(Config c)
        {
            //cfg = (Config)RcSerial.LoadFromFileFast(cfg.GetType(), cfg.XMLFullName);
            cfg = c;            
        }
        
        public void WireupEvents()
        {
            foreach (Feed f in this)
            {
                f.FinishedRefresh +=new EventHandler(f_FinishedRefresh);
            }
        }

        protected override string GetKeyForItem(Feed f)
        {
            return f.Name;
        }
   
        public void ExportDataTable(DataTable dt)
        {
            if (this.Count == 0)
            {
                Feed f = new Feed();
                f.Name = "Test1";
                f.Url = @"http://someurl";
                f.GroupName = "Default";                
                f.KeepCount = 5;
                f.Count =0;
                f.Status = Feed.StatusEnum.Init;
                f.cfg = cfg;
                f.Prepend = false;
                f.DoSync = false;
                f.DoCacheFix = false;
                f.cfg = cfg;
                if (grps != null && f.GroupName != String.Empty)
                    f.group = grps[f.GroupName];               
                this.Add(f);
            }

            dt.Clear();
            //DataTable dt = new DataTable();
            //dt.Columns.Add("Name", typeof(System.String));
            //dt.Columns.Add("Url", typeof(System.String));
            //dt.Columns.Add("GroupName", typeof(System.String));
            //dt.Columns.Add("KeepCount", typeof(System.Int32));
            //dt.Columns.Add("IncludeExt", typeof(System.String));
            //dt.Columns.Add("IncludeText", typeof(System.String));
            //dt.Columns.Add("SavePath", typeof(System.String));

            foreach (Feed f in this)
            {
                DataRow dr = dt.NewRow();
                dr["Name"] = f.Name;
                dr["Url"] = f.Url;
                dr["GroupName"] = f.GroupName;
                dr["KeepCount"] = f.KeepCount;
                dr["IncludeExt"] = f.IncludeExt;
                dr["IncludeText"] = f.IncludeText;
                dr["SavePath"] = f.SavePath;
                dr["Prepend"] = f.Prepend;
                dr["SyncPath"] = f.SyncPath;
                dr["DoSync"] = f.DoSync;
                dr["PrependDate"] = f.PrependDate;
                dr["PrependShortDate"] = f.PrependShortDate;
                dr["DoCacheFix"] = f.DoCacheFix;
                dr["DoDecode"] = f.DoDecode;
                dr["Username"] = f.Username;    
                string pw = f.Password;
                if (!string.IsNullOrEmpty(pw))
                {
                    pw = encryptTag + RcEncryptLite.Encrypt(f.Password);
                }
                dr["Password"] = pw;
                if (f.Priority.HasValue)
                    dr["Priority"] = f.Priority;
                dr["PostDLExe"] = f.PostDLExe;
                dr["PostDLArgs"] = f.PostDLArgs;
                dr["PreSyncExe"] = f.PreSyncExe;
                dr["PreSyncArgs"] = f.PreSyncArgs;
                dr["SuppressSyncCopy"] = f.SuppressSyncCopy;

                dt.Rows.Add(dr);
            }

            //return dt;
        }

        public void ImportDataTable(DataTable dt)
        {
            this.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                Feed f = new Feed();
                f.Name = (string)dr["Name"];
                f.Url = (string)dr["Url"];
                f.GroupName = (string)RcData.RcIsNull(dr,"GroupName","");                
                f.KeepCount = (int)RcData.RcIsNull(dr, "KeepCount", 10);
                f.IncludeExt = (string)RcData.RcIsNull(dr,"IncludeExt","");
                f.IncludeText = (string)RcData.RcIsNull(dr,"IncludeText","");
                f.SavePath = (string)RcData.RcIsNull(dr, "SavePath", "");
                f.Prepend = (bool)RcData.RcIsNull(dr, "Prepend", false);
                f.SyncPath = (string)RcData.RcIsNull(dr, "SyncPath", "");
                f.DoSync = (bool)RcData.RcIsNull(dr, "DoSync", false);
                f.PrependDate = (bool)RcData.RcIsNull(dr, "PrependDate", false);
                f.PrependShortDate = (bool)RcData.RcIsNull(dr, "PrependShortDate", false);
                f.DoCacheFix = (bool)RcData.RcIsNull(dr,"DoCacheFix",false);
                f.DoDecode = (bool)RcData.RcIsNull(dr, "DoDecode", false);
                if (!dr.IsNull("Priority")) 
                    f.Priority = (int)dr["Priority"];
                f.Username = (string)RcData.RcIsNull(dr, "Username", "");
                string pw = (string)RcData.RcIsNull(dr, "Password", "");
                if (!string.IsNullOrEmpty(pw))
                {
                    if (pw.StartsWith(encryptTag))
                    {
                        pw = pw.Replace(encryptTag, "");
                        pw = RcEncryptLite.Decrypt(pw);
                    }
                }
                f.Password = pw;
                f.Status = Feed.StatusEnum.Init;
                f.cfg = cfg;
                if (grps != null && f.GroupName != String.Empty)
                    f.group = grps[f.GroupName];
                f.PostDLExe = (string)RcData.RcIsNull(dr, "PostDLExe", "");
                f.PostDLArgs = (string)RcData.RcIsNull(dr, "PostDLArgs", "");
                f.PreSyncExe = (string)RcData.RcIsNull(dr, "PreSyncExe", "");
                f.PreSyncArgs = (string)RcData.RcIsNull(dr, "PreSyncArgs", "");
                f.SuppressSyncCopy = (bool)RcData.RcIsNull(dr, "SuppressSyncCopy", false);

                this.Add(f);
            }
        }

        public void EncryptPw()
        {
            foreach (Feed f in this)
            {
                if (!string.IsNullOrEmpty(f.Password) && f.Password != RcEncryptLite.PasswordMask)
                    f.Password = RcEncryptLite.Encrypt(f.Password);
            }
        }

        public void DecryptPw()
        {
            foreach (Feed f in this)
            {
                if (!string.IsNullOrEmpty(f.Password))
                    f.Password = RcEncryptLite.Decrypt(f.Password);
            }
        }

        public void ExportMediaCollectionDataTable(DataTable dt)
        {
            foreach (Feed f in this)
            {
                f.MediaCollection.ExportDataTable(dt,f);
            }
        }

        public bool RefreshAll()
        {
            return RefreshAll(true);
        }

        public bool RefreshAll(bool forceAll)
        {
            isStopAll = false;
            if (this.IsRefreshing())
            {
                RcShared.WriteLog("Aborted refreshAll since another feed is currently refreshing.");
                return false;                
            }
            FeedsRefreshed = 0;
            foreach (Feed f in this)
            {
                if (forceAll)
                {
                    f.Status = Feed.StatusEnum.Init;
                }
                f.DumpRawXml = this.DumpRawXml;
                //f.FinishedRefresh += new EventHandler(f_FinishedRefresh);
            }
            RefreshSet(cfg.ScanThread);
            return true;
        }            

        void f_FinishedRefresh(object sender, EventArgs e)
        {
            this.FeedsRefreshed++;
            if (this.ProgressChanged != null)
                this.ProgressChanged(this, new EventArgs());

            if (HasFinishedRefreshAll())
            {
                //don't raise until after final download has completed
                if (!IsRefreshing())
                {
                    //raise the event that all feeds have been refreshed
                    if (this.FinishedRefresh != null)
                        this.FinishedRefresh(this, new EventArgs());
                }
            }
            else
            {
                if (!this.isStopAll)
                {
                    RefreshSet(1); //since one just finished, start up another one
                }
                else
                {
                    if (!IsRefreshing())
                    {
                        //raise the event that all feeds have been refreshed
                        if (this.FinishedRefresh != null)
                            this.FinishedRefresh(this, new EventArgs());
                    }
                }
            }
        }

        private void RefreshSet(int maxThreads)
        {
            int c = 0;
            foreach (Feed f in this)
            {
                if (f.Status == Feed.StatusEnum.Init)
                {
                    c++;
                    f.ConsumeFeed(); //async
                }
                if (c >= maxThreads)
                    return;
            }
        }

        private bool HasFinishedRefreshAll()
        {
            foreach (Feed f in this)
            {
                if (f.Status != Feed.StatusEnum.Refreshed && f.Status != Feed.StatusEnum.Error && f.Status != Feed.StatusEnum.Ignore)
                    return false;
            }
            return true;
        }

        private bool IsRefreshing()
        {
            foreach (Feed f in this)
            {
                if (f.Status == Feed.StatusEnum.Refreshing)
                {
                    return true;
                }                
            }
            return false;
        }

        public void StopAll()
        {
            isStopAll = true;
            foreach (Feed f in this)
            {
                if (f.Status == Feed.StatusEnum.Refreshing)
                {
                    f.Stop();
                }
            }
        }

    }//class
}//namespace

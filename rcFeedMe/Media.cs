﻿/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using RcSoft.RcCommon;
using System.Data;

namespace rcFeedMe
{
    public class Media : ICloneable
    {
        public event EventHandler FinishedDownload;
        public event EventHandler ProgressChanged;

        public enum StatusEnum
        {
            Ignore,
            Queued,
            Downloading,
            Downloaded,
            Error,
            //Stalled,
            //Saved,
            //Deleted,
            Stopped
        }

        public string FeedName = "";      
        public string Title = "";
        public string Summary = "";
        public string Link = "";        
        public string FileName = "";
        public string FullName = "";
        public System.DateTime PubDate;
        public StatusEnum Status = StatusEnum.Ignore;
        public bool DoSave = false;
        public long TotalBytes = 0;
        public bool Synced = false;

        [XmlIgnore]
        public string PostDLExe = "";
        [XmlIgnore]
        public string PostDLArgs = "";
        [XmlIgnore]
        public string PreSyncExe = "";
        [XmlIgnore]
        public string PreSyncArgs = "";
        [XmlIgnore]
        public string GroupName = "";
        [XmlIgnore]
        public string SynchPath = "";
        [XmlIgnore]
        public bool SuppressSyncCopy = false;

        [XmlIgnore]
        public string Username = "";
        [XmlIgnore]
        public string Password = "";

        [XmlIgnore]
        public string SyncFullName = "";
        [XmlIgnore]
        public Config cfg;
        [XmlIgnore]
        public int PercentComplete = 0;
        [XmlIgnore]
        public long BytesReceived = 0;                
        [XmlIgnore]
        public string BytesText
        {
            get
            {
                return BytesReceived.ToString("N0") + " / " + TotalBytes.ToString("N0");
            }
        }        

        public string PK
        {
            get { return FeedName + ";" + FileName; }
        }

        [XmlIgnore]
        public bool InQueue
        {
            get
            {
                if (this.Status == StatusEnum.Ignore)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }             
        }

        [XmlIgnore]
        public DataRow dr = null;

        private int lastPercent = -1;
        //private RcWebAsync request = new RcWebAsync();
        private RcWebAsync request = null;
        private int retryCount = 0;
        [XmlIgnore]
        public const string TempExt = ".rcFeedMeTemp";
        [XmlIgnore]
        public string TempFullName
        {
            get { return FullName + TempExt; }
        }
        [XmlIgnore]
        public string TempName
        {
            get { return FileName + TempExt; }
        }

        public Media()
        {            
        }

        public string ResolveFullName(Feed f, Group g)
        {
            return ResolveFullName(f, g, false);
        }

        public string ResolveFullName(Feed f, Group g, bool skipDirCreate)
        {
            string ret = "";            
            string finalPath = "";            

            finalPath = f.Resolvepath(g);

            if (!skipDirCreate && !Directory.Exists(finalPath))
            {
                Directory.CreateDirectory(finalPath);
            }

            ret = finalPath + @"\" + this.FileName;

            this.FullName = ret;
            return ret;
        }

        public string ResolveSyncFullName(Feed f, Group g)
        {
            string ret = "";
            string finalPath = "";

            finalPath = f.ResolveSyncPath(g);

            if (!Directory.Exists(finalPath))
            {
                Directory.CreateDirectory(finalPath);
            }

            ret = finalPath + @"\" + this.FileName;
            this.SyncFullName = ret;
            return ret;
        }       

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public bool ContainsExt(string ext)
        {
            if (ext == String.Empty)
                return true;
            
            string fext = Path.GetExtension(this.FileName).ToUpper();
            string[] ary = ext.Split("|".ToCharArray());
            foreach (string s in ary)
            {
                if (fext == s.ToUpper())
                {
                    return true;
                }
            }
            return false;
        }

        public bool ContainsText(string str)
        {
            if (str == String.Empty)
                return true;
            string[] ary = str.Split("|".ToCharArray());
            foreach (string s in ary)
            {
                if (this.FileName.ToUpper().Contains(s.ToUpper()))
                {
                    return true;
                }
            }
            return false;
        }

        public void DownloadFile()
        {
            if (request == null)
            {
                request = new RcWebAsync();
                request.DownloadProgressChanged += new System.Net.DownloadProgressChangedEventHandler(request_DownloadProgressChanged);
                request.DownloadFileCompleted += new System.ComponentModel.AsyncCompletedEventHandler(request_DownloadFileCompleted);                        
            }
            lastPercent = -1;
            RcShared.WriteLog("(Media)Started downloading: " + this.FullName);
            this.Status = StatusEnum.Downloading;
            request.TimeoutSec = cfg.DLTimeout;
            request.StallSec = cfg.DLStall;
            request.Retry = cfg.DLRetry;
            if (!request.DownloadFileAsync(this.Link, this.TempFullName,null,this.Username,this.Password))
            {
                this.retryCount = request.RetryCount;
                this.Status = StatusEnum.Error;
                this.RefreshProgress(true);
            }            
            this.retryCount = request.RetryCount;                        
        }

        void request_DownloadProgressChanged(object sender, System.Net.DownloadProgressChangedEventArgs e)
        {            
            if (e.ProgressPercentage != lastPercent)
            {
                this.TotalBytes = e.TotalBytesToReceive;
                this.BytesReceived = e.BytesReceived;
                this.PercentComplete = e.ProgressPercentage;

                RefreshProgress(false);
                lastPercent = this.PercentComplete;
            }

            if (this.ProgressChanged != null)
                this.ProgressChanged(this, new EventArgs());
           // if (this.FileName == @"maxpc_084_20081016.mp3")
            //    this.request.ForceError();
        }

        void request_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            try
            {
                this.retryCount = request.RetryCount;
                //if (this.Status == this.request.Status)
                if (this.request.Status == RcWebAsync.StatusEnum.Aborted)
                {
                    this.Status = StatusEnum.Stopped;
                }
                else
                {
                    this.Status = (Media.StatusEnum)System.Enum.Parse(typeof(StatusEnum), this.request.Status.ToString(), true);
                }
                //zero byte totals are considered errors
                if (this.Status == StatusEnum.Downloaded && this.TotalBytes == 0)
                {
                    this.Status = StatusEnum.Error;
                }
                if (this.Status == StatusEnum.Downloaded && this.TotalBytes != 0 && this.BytesReceived < this.TotalBytes)
                {
                    this.Status = StatusEnum.Error;
                    RcShared.WriteError("Downloaded file seems to be too small: " + this.BytesText);
                }                
                if (this.Status == StatusEnum.Error)
                {
                    string emsg = "";
                    if (e.Error != null)
                        emsg = e.Error.Message;
                    if (e.Error != null && e.Error.InnerException != null)
                        emsg = emsg + "; " + e.Error.InnerException;
                    throw new ApplicationException("File failed to download: " + this.FullName + "; " + emsg);
                }
                else
                {                  
                    RcShared.WriteLog("(Media)Finished download: " + this.FullName);
                    if (File.Exists(this.TempFullName))
                    {
                        if (File.Exists(this.FullName))
                        {
                            RcShared.WriteLog("Old copy of file already exists.  Deleting: " + this.FullName);
                            try
                            {
                                File.Delete(this.FullName);
                            }
                            catch (System.Exception ex)
                            {
                                RcShared.WriteError(ex.Message);
                            }
                        }
                        RcShared.WriteLog("Renaming temp file to the actual filename: " + this.TempFullName + " -> " + this.FullName);
                        try
                        {
                            File.Move(this.TempFullName, this.FullName);
                            FileInfo fi = new FileInfo(this.FullName);
                            if (fi.Exists)
                            {
                                try
                                {
                                    fi.LastWriteTime = this.PubDate;
                                }
                                catch (System.Exception fex)
                                {
                                    RcShared.WriteError("Unable to update timestamp to pubdate: " + this.FullName + "; " + fex.Message);
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {                            
                            File.Delete(this.TempFullName);
                            RcShared.WriteError(ex.Message);
                            RcShared.WriteLog("Unable to rename temp file.  Deleted temp file: " + this.TempFullName);
                        }
                        //download good.  post process                        
                        if (!string.IsNullOrEmpty(this.PostDLExe) && this.Status == StatusEnum.Downloaded)
                        {
                            string args = "";
                            try
                            {
                                if (string.IsNullOrEmpty(this.PostDLArgs))
                                    args = "";
                                else
                                    args = this.PostDLArgs;

                                args = args.Replace(Config.tokenFilename, this.FileName);
                                args = args.Replace(Config.tokenFullname, this.FullName);
                                args = args.Replace(Config.tokenFeedname, this.FeedName);
                                args = args.Replace(Config.tokenSavePath, Path.GetDirectoryName(this.FullName));
                                args = args.Replace(Config.tokenDate, this.PubDate.ToString("yyyyMMdd"));
                                args = args.Replace(Config.tokenSyncPath, this.SynchPath);
                                args = args.Replace(Config.tokenGroupname, this.GroupName);

                                RcShared.WriteLog("Post download exe = " + PostDLExe + "; args = " + args);
                                RcProcess.RunAsyncNoRet(this.PostDLExe, args, System.Diagnostics.ProcessWindowStyle.Hidden);
                            }
                            catch (System.Exception pex)
                            {
                                RcShared.WriteError("Post process failed: " + PostDLExe + "; args = " + args + "; " + pex.Message);
                            }
                        }
                    }
                    this.request = null;                    
                    if (this.FinishedDownload != null)
                        this.FinishedDownload(this, new EventArgs());
                }
            }
            catch (System.Exception ex)
            {
                string inner = "";
                if (ex.InnerException != null)
                    inner = ex.InnerException.Message;
                RcShared.WriteError("(Media)[" + this.FileName + "]" + ex.Message + "; " + inner + "; attempt = " + this.retryCount.ToString());
                if (request != null && request.CanRetry())
                {
                    //this.Status = StatusEnum.Queued;                
                    DownloadFile();
                }
                else
                {
                    RcShared.WriteLog("(Media)Finished download with ERRORS: " + this.FullName);
                    this.request = null;
                    if (this.FinishedDownload != null)
                        this.FinishedDownload(this, new EventArgs());
                }
            }
            finally
            {                
                RefreshProgress(true);
                //RcShared.WriteLog("(Media)Finished download: " + this.FullName);
                //if (this.FinishedDownload != null)
                //    this.FinishedDownload(this, new EventArgs());
            }            
        }

        public void Stop()
        {
            this.Status = StatusEnum.Stopped;
            request.Abort();
        }
      
        public void RefreshProgress(bool forceUpdate)
        {
            if (forceUpdate || this.Status == StatusEnum.Downloading)
            {                    
                if (dr != null)
                {
                    dr["Bytes"] = this.BytesText;
                    dr["Progress"] = this.PercentComplete;
                    dr["Status"] = this.Status.ToString();
                    dr["Attempt"] = this.retryCount;
                }
            }            
        }

        public void GetRelatedObjects(Feeds feeds, Groups groups, out Feed f, out Group g)
        {
            f = null;
            g = null;
            f = feeds[this.FeedName];
            if (f != null)
                g = groups[f.GroupName];
        }
    }
}

﻿/*
 * Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace rcFeedMe
{
    public class MediaFeed : System.Collections.ObjectModel.KeyedCollection<string, Media>
    {
        protected override string GetKeyForItem(Media m)
        {
            return m.PK;
        }

        private int SortByDate(Media m1, Media m2)
        {
            return m2.PubDate.CompareTo(m1.PubDate);            
        }

        public void Sort()
        {
            List<Media> list = base.Items as List<Media>;
            if (list != null)
            {
                list.Sort(SortByDate);
            }
        }

        public bool ContainsNoCase(string pk)
        {
            foreach (Media m in this)
            {
                if (m.PK.ToUpper() == pk.ToUpper())
                    return true;
            }
            return false;
        }

        public void ExportDataTable(DataTable dt, Feed f)
        {

            dt.DefaultView.RowFilter = "Name = '" + f.Name.Replace("'", "''") + "'";
            for (int i = dt.DefaultView.Count - 1; i >= 0; i--)
            {
                dt.DefaultView.Delete(i);
            }

            foreach (Media m in this)
            {
                if (m.FeedName == f.Name)
                {
                    DataRow dr = dt.NewRow();
                    dr["Name"] = m.FeedName;
                    dr["Title"] = m.Title;
                    dr["Summary"] = m.Summary;
                    dr["Link"] = m.Link;
                    dr["InQueue"] = m.InQueue;
                    dr["FileName"] = m.FileName;
                    dr["dtPub"] = m.PubDate;
                    dr["PK"] = m.PK;
                    dt.Rows.Add(dr);
                }
            }
        }
    }
}

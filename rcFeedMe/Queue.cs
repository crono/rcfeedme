﻿/*Copyright 2012 ARogan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RcSoft.RcCommon;
using System.Data;
using System.IO;

namespace rcFeedMe
{
    public class Queue : System.Collections.ObjectModel.KeyedCollection<string, Media>
    {
        public event EventHandler FinishedDownload;
        public event EventHandler ProgressChanged;
        public event EventHandler FinishedFile;

        public const string XMLName = "Queue.xml";
        public readonly string XMLFullName = RcShared.GetExePath(false) + @"\" + Queue.XMLName;

        public int DLCount = 0;

        private bool isStopAll = false;
        private Feeds feedsTemp = null;
        private History historyTemp = null;
        private Groups groupsTemp = null;
        private Config cfgTemp = null;
        private DataTable dtQueueTemp = null;
        private History errorsTemp = null;
        private DataTable dtHistoryTemp = null;
        
        protected override string GetKeyForItem(Media m)
        {
            return m.PK;
        }

        public int Build(Feeds feeds, Groups groups, History history, Config cfg, DataTable dtFeed)
        {
            int cnt = 0;
            foreach (Feed f in feeds)
            {
                if (f.Status == Feed.StatusEnum.Refreshed && f.MediaCollection.Count > 0)
                {                    
                    int mediaCount = 0;
                    Group g = null;
                    if (groups.Contains(f.GroupName))
                        g = groups[f.GroupName];
                    foreach (Media m in f.MediaCollection)
                    {
                        //check count; 0 KeepCount basically disables this feed
                        if (f.KeepCount != -1 && mediaCount >= f.KeepCount)
                        {
                            break;
                        }
                        //substring and extension check
                        if (m.ContainsExt(f.IncludeExt) && m.ContainsText(f.IncludeText))
                        {
                            mediaCount++; //if already in history then count it anyways                                                    
                            //check history          
                            if (!history.Contains(m.PK))                            
                            {                                
                                //see if already in queue                          
                                if (!this.Contains(m.PK))    
                                {                                    
                                    //m.Status = Media.StatusEnum.Queued;
                                    //m.ResolveFullName(f, g);
                                    //m.DoSave = false;
                                    //m.cfg = cfg;
                                    ////this.Add((Media)m.Clone());
                                    //m.FinishedDownload += new EventHandler(m_FinishedDownload);
                                    //m.ProgressChanged += new EventHandler(m_ProgressChanged);
                                    this.AddMedia(m, f, g, cfg, false);
                                    //this.Add(m);
                                    cnt++;
                                    //update datatable
                                    DataRow dr = dtFeed.Rows.Find(m.PK);
                                    if (dr != null)
                                        dr["InQueue"] = m.InQueue;   
                                }
                            }                            
                        }
                    }
                }
            }
            return cnt;
        }

        public bool Process(Feeds feeds, History history, History errors, Groups groups, Config cfg, DataTable dtQueue, DataTable dtHistory)
        {
            isStopAll = false;
            DLCount = 0;

            feedsTemp = feeds;
            groupsTemp = groups;
            historyTemp = history;
            errorsTemp = errors;
            cfgTemp = cfg;
            dtQueueTemp = dtQueue;
            dtHistoryTemp = dtHistory;

            errors.Clear();
            Cleanup();
            if (this.Count == 0)
            {
                TrimFiles();
                return false;
            }
            foreach (Media m in this)
            {
                //if (m.Status == Media.StatusEnum.Queued)
                //{
                //    m.FinishedDownload += new EventHandler(m_FinishedDownload);
                //    m.ProgressChanged += new EventHandler(m_ProgressChanged);
                //}                
                DataRow dr = dtQueue.Rows.Find(m.PK);
                if (dr != null)
                    m.dr = dr;
            }            
            DownloadSet(cfg.DLThread);
            return true;
        }

        public void Cleanup()
        {
            int hcnt = 0;
            int ecnt = 0;
            List<string> wrk = new List<string>();
            foreach (Media m in this)
            {
                if (m.Status == Media.StatusEnum.Downloaded)
                {
                    if (!historyTemp.Contains(m.PK))
                    {
                        //historyTemp.Add((Media)m.Clone());
                        historyTemp.Add(m);
                        hcnt++;
                    }
                    else
                    {
                        if (m.DoSave)
                        {
                            historyTemp[m.PK].DoSave = true;
                        }
                    }
                }

                if (m.Status != Media.StatusEnum.Downloading && m.Status != Media.StatusEnum.Queued)
                {
                    wrk.Add(m.PK);
                }
                if (m.Status == Media.StatusEnum.Error || m.Status == Media.StatusEnum.Stopped)
                {
                    if (File.Exists(m.FullName))
                    {
                        RcShared.WriteLog("Deleting partially downloaded file: " + m.FullName);
                        File.Delete(m.FullName);
                    }
                    if (File.Exists(m.TempFullName))
                    {
                        RcShared.WriteLog("Deleting partially downloaded file: " + m.TempFullName);
                        File.Delete(m.TempFullName);
                    }
                    //errorsTemp.Add((Media)m.Clone());
                    RcShared.WriteLog("File was in error: " + m.FullName);
                    RcShared.WriteLog(RcShared.GetExeBaseName() + "-errors.log", DateTime.Now + "  " + m.Status.ToString() + ": " + m.PK + ";" + m.Title + ";" + m.PubDate.ToString());
                    ecnt++;
                    errorsTemp.Add(m);
                }
            }
            foreach (String s in wrk)
            {
                this.Remove(s);
            }
            historyTemp.Save(dtHistoryTemp);            
            this.ExportDataTable(dtQueueTemp);
            RcShared.WriteLog("(Cleanup)deleted queue items: " + wrk.Count.ToString() + "; moved to history: " + hcnt.ToString() + "; moved to errors: " + ecnt.ToString() );
            if (ecnt > 0)
                RcShared.WriteLog(RcShared.GetExeBaseName() + "-errors.log", "------------------------------------------------");
        }     

        public void TrimFiles()
        {
            try
            {
                int delcnt = 0;
                foreach (Feed f in feedsTemp)
                {
                    //var q = from m in history
                    //        where m.FeedName == f.Name                        
                    //        orderby m.PubDate descending
                    //        select m;

                    //foreach (Media m in q)
                    //{

                    //}

                    if (f.KeepCount == -1)
                        continue;

                    Group g = null;
                    if (groupsTemp.Contains(f.GroupName))
                        g = groupsTemp[f.GroupName];
                    string pth = "";
                    try
                    {
                        pth = f.Resolvepath(g);
                    }
                    catch (System.Exception ex)
                    {
                        RcShared.WriteWarn("(TrimFiles)" + ex.Message);
                        continue;
                    }
                    DirectoryInfo di = new DirectoryInfo(pth);
                    if (!di.Exists)
                        continue;
                    FileInfoCollection fic = new FileInfoCollection(di.GetFiles());
                    //quick check
                    if (fic.Count <= f.KeepCount)
                        continue;
                    fic.SortByLastWriteTime();

                    History h2 = new History();
                    foreach (FileInfo fi in fic)
                    {
                        Media m = historyTemp.FindByFileName(fi.FullName);
                        if (m != null)
                        {
                            if (!m.DoSave)
                                h2.Add(m);
                        }
                    }
                    //check excluding Saved files
                    if (h2.Count <= f.KeepCount)
                        continue;
                    int cnt = h2.Count;
                    h2.SortReverse();
                    foreach (Media m in h2)
                    {
                        try
                        {
                            File.Delete(m.FullName);
                            RcShared.WriteLog("Deleted file: " + m.FullName);
                            delcnt++;
                        }
                        catch (System.Exception ex)
                        {
                            RcShared.WriteError(ex.Message);
                        }
                        cnt--;
                        if (cnt <= f.KeepCount)
                            break;
                    }
                    RcShared.WriteLog("(TrimFiles)Deleted files: " + delcnt.ToString());
                }
            }
            catch (System.Exception ex)
            {
                RcShared.WriteError(ex.Message);
            }
        }
        
        void m_ProgressChanged(object sender, EventArgs e)
        {
            if (this.ProgressChanged != null)
                this.ProgressChanged(this, new EventArgs());
        }

        void m_FinishedDownload(object sender, EventArgs e)
        {
            //Media m = (Media)sender;                            
            //if (m.Status != Media.StatusEnum.Queued)
           // {
                DLCount++;
                if (this.FinishedFile != null)
                    this.FinishedFile(this, new EventArgs());
            //}

            if (FinishedQueue())
            {
                if (!IsDownloading())
                {
                    Cleanup();
                    TrimFiles();
                    //raise the event that all files have been dl
                    if (this.FinishedDownload != null)
                        this.FinishedDownload(this, new EventArgs());
                }
            }
            else
            {
                if (!isStopAll)
                {
                    DownloadSet(1); //since one just finished, start up another one
                }
                else
                {
                    if (!IsDownloading())
                    {
                        Cleanup();
                        TrimFiles();
                        //raise the event that all files have been dl
                        if (this.FinishedDownload != null)
                            this.FinishedDownload(this, new EventArgs());
                    }
                }
            }
        }

        private bool FinishedQueue()
        {            
            foreach (Media m in this)
            {
                if (m.Status == Media.StatusEnum.Queued)
                    return false;
            }
            return true;
        }

        private bool IsDownloading()
        {
            foreach (Media m in this)
            {
                if (m.Status == Media.StatusEnum.Downloading)
                    return true;
            }
            return false;
        }        

        private void DownloadSet(int maxThreads)
        {
            int c = 0;

            //foreach (Media m in this)
            //{
            //    if (m.Status == Media.StatusEnum.Queued)
            //    {
            //        c++;
            //        m.Status = Media.StatusEnum.Downloading;
            //        m.DownloadFile();
            //    }
            //    if (c >= maxThreads)
            //        return;
            //}

            Media m = GetNextDownload();
            while (m != null && c < maxThreads)
            {
                c++;
                m.Status = Media.StatusEnum.Downloading;
                m.DownloadFile();
                m = GetNextDownload();
            }
        }

        //try and spread downloads across different sites to maximize download threads
        private Media GetNextDownload()
        {            
            SortedDictionary<string,int> dlFeeds = new SortedDictionary<string,int>();

            foreach (Media m in this)
            {
                if (m.Status == Media.StatusEnum.Downloading)
                {
                    if (dlFeeds.ContainsKey(m.FeedName))
                    {
                        dlFeeds[m.FeedName]++;
                    }
                    else
                    {                        
                        dlFeeds.Add(m.FeedName,0);
                    }
                }
            }            

            foreach (Media m in this)
            {
                if (m.Status == Media.StatusEnum.Queued)
                {
                    if (!dlFeeds.ContainsKey(m.FeedName))
                    {
                        return m;
                    }
                }
            }

            foreach (Media m in this)
            {
                if (m.Status == Media.StatusEnum.Queued)
                {                    
                    return m;                    
                }
            }
            
            return null;
        }

        public void ExportDataTable(DataTable dt)
        {
            dt.Clear();
         
            foreach (Media m in this)
            {
                DataRow dr = dt.NewRow();
                dr["Name"] = m.FeedName;
                dr["Title"] = m.Title;                
                dr["Link"] = m.Link;
                dr["FileName"] = m.FullName;                
                dr["dtPub"] = m.PubDate;
                dr["Status"] = m.Status.ToString();
                dr["Bytes"] = m.BytesText;
                dr["Progress"] = m.PercentComplete;
                dr["PK"] = m.PK;                
                dt.Rows.Add(dr);
            }
        }

        //public void RefreshProgress(DataTable dt)
        //{
        //    foreach (Media m in this)
        //    {
        //        if (m.NeedsUpdating())
        //        {
        //            DataRow dr = dt.Rows.Find(m.PK);
        //            if (dr != null)
        //            {                     
        //                dr["Bytes"] = m.BytesText;
        //                dr["Progress"] = m.PercentComplete;
        //                dr["Status"] = m.Status.ToString();
        //            }
        //        }
        //    }
        //}

        public void AddMedia(Media m, Feed f, Group g, Config cfg, bool doSave)
        {
            m.Status = Media.StatusEnum.Queued;
            m.DoSave = doSave;
            m.ResolveFullName(f, g);                        
            //this.Add((Media)m.Clone());
            m.FinishedDownload += new EventHandler(m_FinishedDownload);
            m.ProgressChanged += new EventHandler(m_ProgressChanged);
            m.cfg = cfg;
            m.Username = f.Username;
            m.Password = f.Password;
            this.Add(m);
        }

        public void StopAll()
        {
            isStopAll = true;
            foreach (Media m in this)
            {
                if (m.Status == Media.StatusEnum.Downloading)
                {
                    m.Stop();
                }
            }
        }

        public void CleanupTempVars()
        {
            feedsTemp = null;
            historyTemp = null;
            groupsTemp = null;
            cfgTemp = null;
            dtQueueTemp = null;
            errorsTemp = null;
            dtHistoryTemp = null;
        }
     
    }//class
}//namespace

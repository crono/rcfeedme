@echo off

rem
rem Create temp dir where split files will go (subdir of podcast location)
rem
mkdir "%~d1%~p1\temp\"

rem
rem use mp3 split to create smaller chunks
rem
"C:\Program Files\mp3splt_2.4_i386\mp3splt.exe" -f -g [@o,@N=1][@o,@N=2][@o,@N=3][@o,@N=4][@o,@N=5][@o,@N=6] -o @f_@n2  -t 10.0 -d "%~d1%~p1\temp" %1

rem
rem copy files to media for listening.
rem use alphabetical order to ensure correct play order on simple mp3 players
rem
FOR /F %%f IN ('dir /O /B "%~d1%~p1\temp\*.mp3"') DO (
  xcopy /D /Y "%~d1%~p1\temp\%%f" "%2"
  echo Copied "%~d1%~p1\temp\%%f" to "%2"
  erase "%~d1%~p1\temp\%%f"
)